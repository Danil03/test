let timer = 2 * 60 * 60; 

const countdownElement = document.getElementById('countdown');

function updateCountdown() {
    let hours = Math.floor(timer / 3600);
    let minutes = Math.floor((timer % 3600) / 60);
    let seconds = timer % 60;
    
    countdownElement.textContent = `${hours}:${minutes < 10 ? '0' : ''}${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
    
    if (timer > 0) {
        timer--;
        setTimeout(updateCountdown, 1000);
    }
}

updateCountdown();


const phoneInput = document.getElementById('phone');

phoneInput.addEventListener('input', function (e) {
    let x = e.target.value.replace(/\D/g, ''); // Удаляем все нецифровые символы из введённого значения
    let mask = "+38(0__)___-__-__"; // Задаём маску для номера телефона
    let i = 0;
    e.target.value = mask.replace(/[_\d]/g, function (a) {
        return x.charAt(i++) || "_"; // Заменяем пустые места в маске на цифры из введённого номера
    });
});
const dateElement = document.getElementById('date');
const today = new Date();
dateElement.textContent = `Order date: ${today.getDate()}.${today.getMonth() + 1}.${today.getFullYear()}`;